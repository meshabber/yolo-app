#-------------------------------------------------
#
# Project created by QtCreator 2018-07-25T15:25:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Project
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

#LIBS += -L/home/visiongraphiclab/git/darknet -ldarknet
LIBS += -L/home/morning/Git/darknet -ldarknet

#INCLUDEPATH += /home/visiongraphiclab/git/darknet/include
INCLUDEPATH += /home/morning/Git/darknet/include

LIBS += -L/usr/local/cuda-9.0/lib64 \
        -lcudart -lcublas -lcurand -lcudnn

INCLUDEPATH += -I/usr/local/include/opencv
LIBS += -L/usr/local/lib\
        -lopencv_core -lopencv_imgproc -lopencv_highgui \
        -lopencv_videoio -lopencv_imgcodecs

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    YoloLabel.cpp \
    captureqthread.cpp \
    cvimage.cpp \
    cvlabel.cpp

HEADERS += \
        mainwindow.h \
    YoloLabel.h \
    captureqthread.h \
    cvimage.h \
    cvlabel.h

FORMS += \
        mainwindow.ui
