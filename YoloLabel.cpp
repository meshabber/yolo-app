#include "YoloLabel.h"
#include <QDebug>

#include <cstdio>

Yolo::Yolo(QObject * /*parent*/, cv::Mat *sharedBuffer)
    : buffer(sharedBuffer)
{
    alphabet = load_alphabet();
}

Yolo::~Yolo() = default;

void Yolo::setBuffer(cv::Mat *sharedBuffer)
{
    if(sharedBuffer==nullptr) {
        return;
    }

    buffer = sharedBuffer;
}

void Yolo::testDemo()
{
    qDebug() << "Yolo Running";
    emit loadImage(*buffer);
}

void Yolo::demo2()
{
    fps = 1./(what_time_is_it_now() - demo_time);

    demo_time = what_time_is_it_now();

    image im    = cvt_to_image(buffer);
    image sized = letterbox_image(im, net->w, net->h);

    float *X = sized.data;
    // job distributable
    network_predict(net, X);

    int nboxes = 0;
    // job distributable
    detection *dets = get_network_boxes(net, im.w, im.h, demo_thresh, demo_hier, nullptr, 1, &nboxes);
    draw_detect(im, dets, nboxes, demo_thresh);
    free_detections(dets, nboxes);

    cvt_to_cvMat(im);

    free_image(im);
    free_image(sized);
}

void Yolo::netSetup(char *cfg, char *weight, char *object)
{
    list *option = read_data_cfg(object);
    printf("read_data_cfg %s\n", object);

    classes = option_find_int(option, "classes", 20);

    char *name_list = option_find_str(option, "names", "data/names.list");
    names = get_labels(name_list);
    net = load_network(cfg,weight,0);
    set_batch_network(net,1);

    emit sendMessage("===  Convolutional Neural Network Loaded.  ===");
    srand(2222222);
}

void Yolo::cvt_to_cvMat(image img)
{
    cv::Mat mat;
    mat.create(img.h, img.w, CV_8UC3);
    unsigned char *data = mat.ptr();

    if(img.c == 3) rgbgr_image(img);
    auto step = static_cast<int>(mat.step);

    for(int y=0; y<img.h; ++y){
        for(int x=0; x<img.w; ++x){
            for(int k=0; k<img.c; ++k){
                data[y*step + x*img.c + k] = static_cast<unsigned char>(get_pixel(img,x,y,k)*255);
            }
        }
    }
    emit loadImage(mat);
}

///////
image Yolo::cvt_to_image(cv::Mat *mat)
{
    if(mat->empty() || mat == nullptr) return make_image(0,0,0); //empty_image;

    const auto h = static_cast<size_t>(mat->rows);
    const auto w = static_cast<size_t>(mat->cols);
    const auto c = static_cast<size_t>(mat->channels());
    //const auto step = buffer->step;

    image im = make_image(static_cast<int>(w),
                          static_cast<int>(h),
                          static_cast<int>(c));

    auto data = mat->ptr();

    //copy to..
    for(size_t i = 0; i<h; ++i){
        for(size_t k=0; k<c; ++k){
            for(size_t j=0; j<w; ++j){
                im.data[k*w*h + i*w + j] = static_cast<float>( data[i*mat->step + j*c + k]/255. );
            }
        }
    }

    return im;
}

////////


inline float Yolo::get_pixel(image m, int x, int y, int c)
{
    assert(x<m.w && y<m.h && c<m.c);
    return m.data[c*m.h*m.w + y*m.w + x];
}

void Yolo::draw_detect(image im, detection *dets, int num, float thresh)
{
    int i,j;
    for(i = 0; i < num; ++i){
        char labelstr[4096] = {0};
        int _class = -1;
        for(j = 0; j < classes; ++j){
            if (dets[i].prob[j] > thresh){
                if (_class < 0) {
                    strcat(labelstr, names[j]);
                    _class = j;
                } else {
                    strcat(labelstr, ", ");
                    strcat(labelstr, names[j]);
                }
                emit loadResult(names[j], dets[i].prob[j]*100, fps);
            }
        }
        if(_class >= 0){

            float rgb[3]  {
                get_color(2,_class,classes), //red
                get_color(1,_class,classes), //green
                get_color(0,_class,classes)  //blue
            };

            box b = dets[i].bbox;

            int left  = (left < 0)		 ? 0	  : (b.x-b.w/2.)*im.w;
            int right = (right > im.w-1) ? im.w-1 : (b.x+b.w/2.)*im.w;
            int top   = (top < 0) 		 ? 0	  : (b.y-b.h/2.)*im.h;
            int bot   = (bot > im.h-1) 	 ? im.h-1 : (b.y+b.h/2.)*im.h;

            int width = im.h * .006;
            draw_box_width(im, left, top, right, bot, width, rgb[0]/*red*/, rgb[1]/*green*/, rgb[2]/*blue*/);
            if (alphabet != nullptr) {
                image label = get_label(alphabet, labelstr, (im.h*.03));
                draw_label(im, top + width, left, label, rgb);
                free_image(label);
            }
            if (dets[i].mask != nullptr){
                image mask = float_to_image(14, 14, 1, dets[i].mask);
                image resized_mask = resize_image(mask, b.w*im.w, b.h*im.h);
                image tmask = threshold_image(resized_mask, .5);
                embed_image(tmask, im, left, top);
                free_image(mask);
                free_image(resized_mask);
                free_image(tmask);
            }
        }

    }
}

float Yolo::get_color(int c, int cls, int max)
{
    int hash = cls*123457 % max;
    float ratio = (hash/max)*5;
    auto i = static_cast<int>(floor(ratio));
    auto j = static_cast<int>(ceil(ratio));
    ratio -= i;
    float r = (1-ratio) * colors[i][c] + ratio*colors[j][c];

    return r;
}

void Yolo::embed_image(image source, image dest, int dx, int dy)
{
    int x,y,k;
    for(k = 0; k < source.c; ++k){
        for(y = 0; y < source.h; ++y){
            for(x = 0; x < source.w; ++x){
                float val = get_pixel(source, x,y,k);
                set_pixel(dest, dx+x, dy+y, k, val);
            }
        }
    }
}

inline void Yolo::set_pixel(image m, int x, int y, int c, float val)
{
    if (x < 0 || y < 0 || c < 0 || x >= m.w || y >= m.h || c >= m.c) return;
    assert(x < m.w && y < m.h && c < m.c);
    m.data[c*m.h*m.w + y*m.w + x] = val;
}


