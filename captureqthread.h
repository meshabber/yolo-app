#ifndef CAPTUREQTHREAD_H
#define CAPTUREQTHREAD_H

#include <QThread>
#include <QMutex>

namespace cv{
class Mat;
class VideoCapture;
};

class CaptureQThread : public QThread
{
    Q_OBJECT
public:
    CaptureQThread() = default;
    CaptureQThread(const std::string & srcPath ,cv::Mat * sharedBuffer);
    ~CaptureQThread() override;

    void set(const std::string &srcPath, cv::Mat *sharedBuffer);

private:
    cv::VideoCapture *cap;
    cv::Mat			 *buffer;

    unsigned long w_time=10;
    QMutex mutex;

protected:
    void run() override;

signals:
    void updateImage();
};

#endif // CAPTUREQTHREAD_H
