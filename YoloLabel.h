#ifndef YOLOLABEL_H
#define YOLOLABEL_H

#include <QObject>

extern "C"
{
    #include <darknet.h>
}
#include <opencv2/opencv.hpp>

class Yolo : public QObject
{
    Q_OBJECT
public:
    explicit Yolo(QObject *parent = nullptr, cv::Mat * sharedBuffer=nullptr);
    ~Yolo() override;

    void setBuffer(cv::Mat *);
    void netSetup(char * cfg, char * weight, char * object);

signals:
    void loadImage(cv::Mat mat);
    void loadResult(char *name, float th, float f);
    void sendMessage(QString str);

public slots:
    void testDemo();
    void demo2();

private:
    cv::Mat *buffer = nullptr;

    float fps;

    int classes;
    char **names;
    network *net;
    image **alphabet;

    const float demo_thresh = .3f;
    const float demo_hier = .5f;
    const float colors[6][3] = { {1,0,1}, {0,0,1},{0,1,1},{0,1,0},{1,1,0},{1,0,0} };
    double demo_time;

    QString result;

    inline image cvt_to_image(cv::Mat *);
    inline void  cvt_to_cvMat(image img);

    void draw_detect(image im, detection *dets, int num, float thresh);
    void embed_image(image source, image dest, int dx, int dy);

    inline float get_color(int c, int cls, int max);
    inline float get_pixel(image m, int x, int y, int c);
    inline void  set_pixel(image m, int x, int y, int c, float val);

};


#endif // YOLOLABEL_H
