#include "captureqthread.h"

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>

#include <QDebug>

CaptureQThread::CaptureQThread(const std::string & srcPath, cv::Mat * sharedBuffer) :
      buffer(sharedBuffer)
{
    cap = new cv::VideoCapture(srcPath);
    if(!cap->isOpened()) { this->exit(-1); }
}

CaptureQThread::~CaptureQThread()
{
    delete cap;
}

void CaptureQThread::set(const std::string &srcPath, cv::Mat *sharedBuffer)
{
    if(nullptr!=cap) delete cap;
    cap = new cv::VideoCapture(srcPath);

    buffer = sharedBuffer;
}

void CaptureQThread::run()
{
    for(;;)
    {
        mutex.lock();
        //need some pause stat..

        if(!cap->read(*buffer)) { break; }
        emit updateImage();
        mutex.unlock();

        msleep(w_time);
    }
}
