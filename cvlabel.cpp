#include "cvlabel.h"

//#include <opencv2/core.hpp>

#include "cvimage.h"

#include <QDebug>

CVLabel::CVLabel(QLabel *parent) :
    QLabel(parent)
{
    this->resize(parent->size());
}

void CVLabel::setCVImage(const CVImage &cvimg)
{
    buf = cvimg.toQImage().scaled(this->size(), Qt::KeepAspectRatio);

    setPixmap(QPixmap::fromImage(buf));
}

void CVLabel::setImage(const cv::Mat &mat)
{
    buf = CVImage::mat2qimg(mat).scaled(this->size(), Qt::KeepAspectRatio);

    setPixmap(QPixmap::fromImage(buf));
}
