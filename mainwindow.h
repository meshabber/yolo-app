#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
extern "C"
{
    #include <darknet.h>
}

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <QImage>
#include <YoloLabel.h>
#include <QFileDialog>
#include <QTimer>
#include <QThread>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

#include "cvlabel.h"
#include "captureqthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    enum class NetConfig{
        CFG,
        WEIGHT,
        OBJECT
    };

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void updateResult(char *name, float th, float f);
    void updateMessage(QString str);

private:
    void onClick_net(NetConfig ncfg);
    void openVideo();
    void disableBtn();
    void enableBtn();
    void updateLabel(QString str);

    Ui::MainWindow *ui;
    CVLabel *lbl_img;
    CaptureQThread * i_stream = nullptr;
    cv::Mat buffer;

    QMessageBox *msg;
    QThread *yoloThread;
    Yolo *yololabel;

    int warnig;
signals:
};

#endif // MAINWINDOW_H
