#include "cvimage.h"

CVImage::CVImage(const cv::Mat &mat)
    : cv::Mat(mat)
{

}

//CVImage &CVImage::operator=(const cv::Mat &mat)
//{
//    cv::Mat(mat.clone());
//    return *this;
//}

CVImage::~CVImage()
{
    ~cv::Mat();
}

QImage CVImage::mat2qimg(const cv::Mat &mat)
{
    QImage image;

    switch (mat.type())
    {
    case CV_8UC4:
        image = QImage(mat.data, mat.cols, mat.rows, static_cast<int>(mat.step),
                       QImage::Format_RGB32);
        break;
    case CV_8UC3:
        image = QImage(mat.data, mat.cols, mat.rows, static_cast<int>(mat.step),
                       QImage::Format_RGB888);
        image = image.rgbSwapped();
        break;
    case CV_8UC1:
        static QVector<QRgb> colorTable;
        if(colorTable.isEmpty()){
            for(int i=0;i<256;i++){
                colorTable.push_back(qRgb(i,i,i));
            }
        }
        image = QImage(mat.data, mat.cols, mat.rows, static_cast<int>(mat.step),
                       QImage::Format_Indexed8);
        image.setColorTable(colorTable);
        break;
    default: //returning null image
        break;
    }

    return image;
}

const QImage CVImage::toQImage() const
{
    return CVImage::mat2qimg(*this);
}
