﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "YoloLabel.h"
#include <QTimer>
#include <QDebug>
#include <QMessageBox>
#include <QStyleFactory>

#include "cvimage.h"

#include <cstdio>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    

    yololabel = new Yolo();
    yololabel->setBuffer(&buffer);
    yoloThread = new QThread;

    lbl_img = new CVLabel(ui->img_lb);
    i_stream = new CaptureQThread();

    yololabel->moveToThread(yoloThread);
    yoloThread->start();

//    connect(i_stream, &CaptureQThread::updateImage,
//            [=](){ lbl_img->setImage( buffer ); });

    msg = new QMessageBox();
    msg->setWindowTitle("Warning");
    msg->setIcon(QMessageBox::Warning);
    msg->setModal(false);
    disableBtn();

    connect(i_stream, &CaptureQThread::updateImage, yololabel,  &Yolo::demo2);
    connect(i_stream, &CaptureQThread::finished,    yoloThread, &QThread::quit);

    connect(yololabel, &Yolo::loadImage,   [=](cv::Mat mat){ lbl_img->setImage(mat); });
    connect(yololabel, &Yolo::loadResult,  this, &MainWindow::updateResult);
    connect(yololabel, &Yolo::sendMessage, this, &MainWindow::updateMessage);

    connect(ui->filename_btn, &QPushButton::clicked, [=](){ openVideo(); });
    connect(ui->cfg_btn,      &QPushButton::clicked, [=](){ onClick_net(NetConfig::CFG);    });
    connect(ui->weights_btn,  &QPushButton::clicked, [=](){ onClick_net(NetConfig::WEIGHT); });
    connect(ui->data_btn,     &QPushButton::clicked, [=](){ onClick_net(NetConfig::OBJECT); });
    connect(ui->netsetup_btn, &QPushButton::clicked,
            [=](){
        QString cfg    = ui->cfg_text->text();
        QString weight = ui->weights_text->text();
        QString object = ui->data_text->text();

        if( cfg.isEmpty() || weight.isEmpty() || object.isEmpty() ){
            msg->setText( "Check File path plz.." );
            msg->show();
            return;
        }

        yololabel->netSetup(const_cast<char *>(cfg.toStdString().c_str()),
                            const_cast<char *>(weight.toStdString().c_str()),
                            const_cast<char *>(object.toStdString().c_str()));

//        yololabel->netSetup("/home/visiongraphiclab/0810/firesmoke.cfg",
//                            "/home/visiongraphiclab/0810/firesmoke.backup",
//                            "/home/visiongraphiclab/0810/obj.data");

        enableBtn();
    });
    connect(ui->ipcam_btn, &QPushButton::clicked,
            [=](){
    });

    connect(ui->Video, &QRadioButton::clicked,
            [=](){
        ui->video_text->setEnabled(true);
        ui->filename_btn->setEnabled(true);
        ui->ipcam_text->setEnabled(false);
        ui->ipcam_btn->setEnabled(false);
    });
    connect(ui->IPCAM, &QRadioButton::clicked,
            [this](){
        ui->video_text->setEnabled(false);
        ui->filename_btn->setEnabled(false);
        ui->ipcam_text->setEnabled(true);
        ui->ipcam_btn->setEnabled(true);
    });

    connect(ui->Warning, &QSlider::valueChanged, [=](int i){ warnig = i; });

    connect(ui->yolo_start, &QPushButton::clicked,
            [=](){
        while(i_stream->isRunning()) i_stream->terminate();

        i_stream->set(ui->video_text->text().toStdString(), &buffer);
        i_stream->start();
    });

    connect(ui->yolo_stop, &QPushButton::clicked,
            [=](){
    });
}

MainWindow::~MainWindow()
{
    yoloThread->terminate();
    i_stream->terminate();

    delete ui;
    delete lbl_img;
    delete yololabel;
    delete yoloThread;
    delete i_stream;
    delete msg;
}

void MainWindow::onClick_net(NetConfig ncfg)
{
    QFileDialog fdlg;
//    QString path = QCoreApplication::applicationDirPath();
    QString path = "/home/visiongraphiclab/0810/";
    QString filename;
    QString filter;

    QLineEdit * target;

    switch(ncfg)
    {
    case NetConfig::CFG :
        filter = "Cfg Files(*.cfg)";
        target = ui->cfg_text;
        break;
    case NetConfig::WEIGHT :
        filter = "Backup Files(*.backup);; Weights File(*.weights)";
        target = ui->weights_text;
        break;
    case NetConfig::OBJECT :
        filter = "Data File(*.data)";
        target = ui->data_text;
        break;
    }

    filename = fdlg.getOpenFileName(this, "File Open", path,
                               filter);

    qDebug() << filename;

    target->setText(filename);
}

void MainWindow::openVideo()
{
    QFileDialog fdlg;
    QString path = "/home/visiongraphiclab/TestVideo/";
    QString filename;
    filename = fdlg.getOpenFileName(this, "File Open", path,
                               "All File(*)");
    qDebug() << filename;
    ui->video_text->setText(filename);
}

void MainWindow::updateResult(char *name, float th, float f)
{
    QString str;
    QString str2;
    //str2.sprintf("FPS: %.0f",f);
    str.sprintf("FPS:  %.0f   Object:   %s   %.0f%% \n", f,name, th);
    str2.sprintf("Object:   %s   %.0f%%",name, th);
    if(strcmp(name,"fire") == 0){
        ui->textEdit->setTextColor(QColor("red"));
    }
    else{
        ui->textEdit->setTextColor(QColor("blue"));
    }
    ui->textEdit->append(str);
    //ui->smkdetect->append(in->readLine());
    if(th > warnig){
        msg->setText(str2);
        msg->show();
    }
}

void MainWindow::updateMessage(QString str)
{
    ui->textEdit->append(str);
}

void MainWindow::updateLabel(QString str)
{
    QString tmp("Open File: ");
    tmp.append(str);
    ui->video_info->setText(str);
    ui->textEdit->append(tmp);
}

void MainWindow::disableBtn()
{
    ui->CAMset->setEnabled(false);
    ui->Warningset->setEnabled(false);
    ui->yolo_start->setEnabled(false);
    ui->yolo_stop->setEnabled(false);
}

void MainWindow::enableBtn()
{
    ui->CAMset->setEnabled(true);
    ui->Warningset->setEnabled(true);
    ui->yolo_start->setEnabled(true);
    ui->yolo_stop->setEnabled(true);
}
