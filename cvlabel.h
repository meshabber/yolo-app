#ifndef CVLABEL_H
#define CVLABEL_H

#include <QLabel>

namespace cv {
class Mat;
}

class CVImage;

class CVLabel : public QLabel
{
    Q_OBJECT
public:
    explicit CVLabel(QLabel *parent = nullptr);

public slots:
    void setCVImage(const CVImage &);
    void setImage(const cv::Mat &);

private:
    QImage buf;
};

#endif // CVLABEL_H
